import { Module } from '@nestjs/common';
import { EventModule } from './event/event.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventEntity } from './event/entity/event.entity';
import { UserModule } from './user/user.module';
import { UserEntity } from './user/entity/user.entity';
import { AuthModule } from './auth/auth.module';
import { ParticipantEntity } from './event/entity/participant.entity';
import { GraphQLModule } from '@nestjs/graphql';
import { PostEntity } from './event/entity/post.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'aim',
      password: 'aim',
      database: 'confetti',
      entities: [EventEntity, ParticipantEntity, UserEntity, PostEntity],
    }),
    GraphQLModule.forRoot({
      autoSchemaFile: 'schema.gql',
      context: ({ req }) => ({ req }),
    }),
    EventModule,
    UserModule,
    AuthModule,
  ],
})
export class AppModule {}
