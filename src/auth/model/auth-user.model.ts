import { Role } from '../../commons/constants/role';

export interface AuthUserModel {
  id: number;
  email: string;
  role: Role;
}
