import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthUserModel } from '../../model/auth-user.model';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../../../user/entity/user.entity';
import { Repository } from 'typeorm';
import { RegistrationModel } from '../../model/registration.model';
import { Role } from '../../../commons/constants/role';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(username: string, pass: string): Promise<AuthUserModel> {
    const user = await this.userRepository.findOne({ email: username });
    if (user && user.password === pass) {
      return {
        id: user.id,
        email: user.email,
        role: user.role,
      };
    }
    return null;
  }

  async login(user: AuthUserModel) {
    const payload = { username: user.email, id: user.id, role: user.role };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  register(user: RegistrationModel) {
    const ent = {
      ...user,
      role: Role.USER,
    };
    return this.userRepository.save(ent);
  }
}
