import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ParticipantEntity } from './participant.entity';
import { PostEntity } from './post.entity';

@Entity({ name: 'confetti.event' })
export class EventEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column({ type: 'bytea' })
  photo: string;

  @Column({ name: 'start_date' })
  startDate: Date;

  @OneToMany(
    type => ParticipantEntity,
    participant => participant.event,
    { cascade: true },
  )
  participants: ParticipantEntity[];

  @OneToMany(
    type => PostEntity,
    post => post.event,
    { cascade: true },
  )
  posts: PostEntity[];
}
