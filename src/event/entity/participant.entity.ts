import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { EventEntity } from './event.entity';
import { UserEntity } from '../../user/entity/user.entity';

@Entity('confetti.participant')
export class ParticipantEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  manager: boolean;

  @ManyToOne(
    type => EventEntity,
    event => event.participants,
    { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
  )
  @JoinColumn({ name: 'event_id' })
  event: EventEntity;

  @ManyToOne(
    type => UserEntity,
    user => user.participations,
  )
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;
}
