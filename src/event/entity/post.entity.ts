import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { EventEntity } from './event.entity';
import { UserEntity } from '../../user/entity/user.entity';

@Entity('confetti.post')
export class PostEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  content: string;

  @Column()
  posted: Date;

  @ManyToOne(
    type => EventEntity,
    event => event.participants,
    { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
  )
  @JoinColumn({ name: 'event_id' })
  event: EventEntity;

  @ManyToOne(
    type => UserEntity,
    user => user.participations,
  )
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;
}
