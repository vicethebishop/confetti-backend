import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventEntity } from './entity/event.entity';
import { EventService } from './service/event.service';
import { AuthModule } from '../auth/auth.module';
import { ParticipantEntity } from './entity/participant.entity';
import { EventResolver } from './resolver/event.resolver';
import { PostEntity } from './entity/post.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([EventEntity, ParticipantEntity, PostEntity]),
    AuthModule,
  ],
  providers: [EventService, EventResolver],
  exports: [EventService],
})
export class EventModule {}
