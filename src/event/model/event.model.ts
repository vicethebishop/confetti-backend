import { Field, Int, ObjectType } from 'type-graphql';
import { ParticipantModel } from './participant.model';
import { PostModel } from './post.model';

@ObjectType('event')
export class EventModel {
  @Field(type => Int)
  id: number;

  @Field()
  title: string;

  @Field({ nullable: true })
  description: string;

  @Field({ nullable: true })
  startDate: Date;

  @Field()
  currentIsParticipant: boolean;

  @Field()
  currentIsManager: boolean;

  @Field(type => [ParticipantModel], { nullable: true })
  participants: ParticipantModel[];

  @Field(type => [PostModel], { nullable: true })
  posts: PostModel[];
}
