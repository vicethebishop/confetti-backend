import { Order } from '../../commons/constants/order';
import { ArgsType, Field } from 'type-graphql';

@ArgsType()
export class GetEventArgs {
  @Field({ nullable: true })
  sortBy: string;

  @Field({ nullable: true })
  order: Order;
}
