import { Field, Int, ObjectType } from 'type-graphql';
import { UserEntity } from '../../user/entity/user.entity';
import { UserModel } from '../../user/model/user.model';

@ObjectType()
export class ParticipantModel {
  @Field(type => Int)
  id: number;

  @Field()
  manager: boolean;

  @Field(type => UserModel)
  user: UserEntity;
}
