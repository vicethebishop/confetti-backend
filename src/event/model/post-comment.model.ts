import { Field, InputType } from 'type-graphql';

@InputType()
export class PostCommentModel {
  @Field() eventId: number;
  @Field() content: string;
}
