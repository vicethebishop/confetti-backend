import { Field, Int, ObjectType } from 'type-graphql';
import { UserModel } from '../../user/model/user.model';
import { UserEntity } from '../../user/entity/user.entity';

@ObjectType()
export class PostModel {
  @Field(type => Int)
  id: number;

  @Field()
  content: string;

  @Field()
  posted: Date;

  @Field(type => UserModel)
  user: UserEntity;
}
