import { Field, InputType } from 'type-graphql';

@InputType()
export class UpdateEventInputModel {
  @Field() id: number
  @Field() title: string;
  @Field({ nullable: true }) description?: string;
  @Field({ nullable: true }) startDate: Date;
}
