import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Arg, Int } from 'type-graphql';
import { EventService } from '../service/event.service';
import { EventModel } from '../model/event.model';
import { UseGuards } from '@nestjs/common';
import { CurrentUser, GqlAuthGuard } from '../../guards/gql-auth.guard';
import { EventEntity } from '../entity/event.entity';
import { CreateEventInputModel } from '../model/create-event-input.model';
import { GetEventArgs } from '../model/get-event-args';
import { PostCommentModel } from '../model/post-comment.model';
import { UpdateEventInputModel } from '../model/update-event-input.model';

@UseGuards(GqlAuthGuard)
@Resolver('event')
export class EventResolver {
  constructor(private readonly service: EventService) {}

  @Query(returns => [EventModel])
  async events(@Args() args: GetEventArgs, @CurrentUser() user) {
    return this.service
      .getAll(args)
      .then(value => value.map(event => this.enhance(event, user.id)));
  }

  @Query(returns => [EventModel])
  async ownEvents(@Args() args: GetEventArgs, @CurrentUser() user) {
    return this.service
      .getOwn(args, user.id)
      .then(value => value.map(event => this.enhance(event, user.id)));
  }

  @Query(returns => EventModel, { nullable: true })
  async event(
    @Args({ name: 'id', type: () => Int }) id: number,
    @CurrentUser() user,
  ) {
    return this.service.getById(id).then(value => {
      if (value) {
        return this.enhance(value, user.id);
      }
      return value;
    });
  }

  @Mutation(returns => EventModel)
  async participation(
    @Args({ name: 'id', type: () => Int }) id: number,
    @CurrentUser() user,
  ) {
    return this.service.changeParticipation(id, user.id).then(value => {
      if (value) {
        return this.enhance(value, user.id);
      }
      return value;
    });
  }

  @Mutation(returns => EventModel)
  async post(
    @Args({ name: 'model', type: () => PostCommentModel })
    model: PostCommentModel,
    @CurrentUser() user,
  ) {
    return this.service
      .postComment(model.eventId, user.id, model.content)
      .then(value => {
        if (value) {
          return this.enhance(value, user.id);
        }
        return value;
      });
  }

  @Mutation(returns => EventModel)
  async createEvent(
    @Args({ name: 'model', type: () => CreateEventInputModel })
    model: CreateEventInputModel,
    @CurrentUser() user,
  ) {
    return this.service
      .create(model, user.id)
      .then(value => this.enhance(value, user.id));
  }

  @Mutation(returns => EventModel)
  async updateEvent(
    @Args({ name: 'model', type: () => UpdateEventInputModel })
      model: UpdateEventInputModel,
    @CurrentUser() user,
  ) {
    return this.service
      .update(model)
      .then(value => this.enhance(value, user.id));
  }

  private enhance(entity: EventEntity, userId: number): any {
    return {
      ...entity,
      currentIsParticipant:
        !!entity.participants &&
        entity.participants.some(part => part.user.id === userId),
      currentIsManager:
        !!entity.participants &&
        entity.participants.some(
          part => part.user.id === userId && part.manager,
        ),
    };
  }
}
