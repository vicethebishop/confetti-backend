import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventEntity } from '../entity/event.entity';
import { Repository } from 'typeorm';
import { ParticipantEntity } from '../entity/participant.entity';
import { UserEntity } from '../../user/entity/user.entity';
import { CreateEventInputModel } from '../model/create-event-input.model';
import { GetEventArgs } from '../model/get-event-args';
import { PostEntity } from '../entity/post.entity';
import { UpdateEventInputModel } from '../model/update-event-input.model';

@Injectable()
export class EventService {
  constructor(
    @InjectRepository(EventEntity)
    private readonly repository: Repository<EventEntity>,
    @InjectRepository(ParticipantEntity)
    private readonly partRepository: Repository<ParticipantEntity>,
    @InjectRepository(PostEntity)
    private readonly postRepository: Repository<PostEntity>,
  ) {}

  getById(id: number) {
    return this.repository.findOne(id, {
      join: {
        alias: 'event',
        leftJoinAndSelect: {
          users: 'event.participants',
          user: 'users.user',
          posts: 'event.posts',
          post: 'posts.user',
        },
      },
    });
  }

  getAll(sort: GetEventArgs) {
    return this.repository.find({
      order: {
        [sort.sortBy]: sort.order,
      },
    });
  }

  getOwn(sort: GetEventArgs, userId: number) {
    return this.repository
      .find({
        order: {
          [sort.sortBy]: sort.order,
        },
        join: {
          alias: 'event',
          leftJoinAndSelect: {
            users: 'event.participants',
            user: 'users.user',
          },
        },
      })
      .then(value =>
        value.filter(event =>
          event.participants.some(part => part.user.id === userId),
        ),
      );
  }

  async create(model: CreateEventInputModel, userId: number) {
    let event = new EventEntity();
    event.title = model.title;
    event.description = model.description;
    event.startDate = model.startDate;
    event = await this.repository.save(event);

    const part = new ParticipantEntity();
    part.event = event;
    part.user = new UserEntity();
    part.user.id = userId;
    part.manager = true;

    await this.partRepository.save(part);
    return this.getById(event.id);
  }

  async update(model: UpdateEventInputModel) {
    const event = new EventEntity();
    event.title = model.title;
    event.description = model.description;
    event.startDate = model.startDate;
    await this.repository.update(model.id, event);
    return this.getById(model.id);
  }

  async changeParticipation(eventId: number, userId: number) {
    const event = await this.getById(eventId);
    if (event) {
      const part = event.participants.find(value => value.user.id === userId);
      if (part) {
        const partId = part.id;
        await this.partRepository.delete(partId);
      } else {
        const newPart = new ParticipantEntity();
        newPart.user = new UserEntity();
        newPart.event = new EventEntity();
        newPart.user.id = userId;
        newPart.event.id = eventId;
        newPart.manager = false;

        await this.partRepository.save(newPart);
      }

      return this.getById(eventId);
    }
  }

  async postComment(eventId: number, userId: number, content: string) {
    const event = await this.getById(eventId);
    if (event) {
      const post = new PostEntity();
      post.user = new UserEntity();
      post.event = new EventEntity();
      post.user.id = userId;
      post.event.id = eventId;
      post.content = content;
      post.posted = new Date();
      await this.postRepository.save(post);
    }

    return this.getById(eventId);
  }
}
