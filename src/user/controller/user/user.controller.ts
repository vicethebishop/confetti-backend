import {
  Controller,
  Get,
  Post,
  Res,
  UploadedFile,
  UseInterceptors,
  UseGuards,
  Param,
  Put,
  Req,
  Body,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { UserService } from '../../service/user.service';
import { Response } from 'express';
import { Readable } from 'stream';
import { AuthGuard } from '@nestjs/passport';

@Controller('user')
@UseGuards(AuthGuard('jwt'))
export class UserController {
  constructor(private readonly service: UserService) {}

  @Get(':id/photo/')
  async getPhoto(@Res() res: Response, @Param('id') id: number) {
    const buffer = await this.service.getPhoto(id);
    const stream = new Readable();
    if (buffer) {
      stream.push(buffer);

      res.set({
        'Content-Type': 'application/octet-stream',
        'Cache-Control': 'no-cache, must-revalidate, private, maxage=0',
        'Content-Length': buffer.length,
      });
    } else {
      res.set({
        'Content-Type': 'application/octet-stream',
        'Content-Length': 0,
      });
    }
    stream.push(null);

    stream.pipe(res);
  }

  @Post(':id/photo')
  @UseInterceptors(FileInterceptor('photo'))
  uploadPhoto(@UploadedFile() photo, @Param('id') id: number) {
    this.service.savePhoto(photo, id);
  }

  @Put('email')
  changeEmail(@Body('newEmail') newEmail, @Req() req) {
    this.service.changeEmail(req.user.id, newEmail);
  }

  @Put('password')
  async changePassword(
    @Body('oldPassword') oldPassword: string,
    @Body('newPassword') newPassword: string,
    @Req() req,
  ) {
    const oldPasswordIsValid = await this.service.checkPassword(
      req.user.id,
      oldPassword,
    );
    if (!oldPasswordIsValid) {
      throw new HttpException(
        'Old password in invalid',
        HttpStatus.UNAUTHORIZED,
      );
    }

    return this.service.changePassword(req.user.id, newPassword);
  }
}
