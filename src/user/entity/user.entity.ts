import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Role } from '../../commons/constants/role';
import { Exclude } from 'class-transformer';
import { ParticipantEntity } from '../../event/entity/participant.entity';

@Entity('confetti.user')
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Exclude({
    toPlainOnly: true,
  })
  @Column()
  password: string;

  @Column({ name: 'first_name' })
  firstName: string;

  @Column({ name: 'last_name' })
  lastName: string;

  @Column()
  bio: string;

  @Column()
  photo: string;

  @Column({ type: 'enum' })
  role: Role;

  @OneToMany(
    type => ParticipantEntity,
    participant => participant.user,
  )
  participations: ParticipantEntity[];
}
