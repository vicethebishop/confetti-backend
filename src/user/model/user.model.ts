import { Field, Int, ObjectType } from 'type-graphql';
import { Role } from '../../commons/constants/role';

@ObjectType('user')
export class UserModel {
  @Field(type => Int)
  id: number;

  @Field()
  firstName: string;

  @Field()
  lastName: string;

  @Field()
  role: Role;

  @Field({ nullable: true })
  bio: string;
}
