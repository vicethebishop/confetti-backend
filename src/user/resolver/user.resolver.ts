import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UserService } from '../service/user.service';
import { UserModel } from '../model/user.model';
import { Int } from 'type-graphql';
import { EventModel } from '../../event/model/event.model';
import { CreateEventInputModel } from '../../event/model/create-event-input.model';
import { CurrentUser } from '../../guards/gql-auth.guard';

// @UseGuards(GqlAuthGuard)
@Resolver('user')
export class UserResolver {
  constructor(private readonly service: UserService) {}

  @Query(returns => [UserModel])
  async users() {
    return this.service.getAll();
  }

  @Query(returns => UserModel, { nullable: true })
  async user(
    @Args({ name: 'id', type: () => Int }) id: number,
  ): Promise<UserModel> {
    return this.service.get(id);
  }
}
