import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../entity/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly repository: Repository<UserEntity>,
  ) {}

  public getAll() {
    return this.repository.find();
  }

  public get(id: number) {
    return this.repository.findOne(id);
  }

  public savePhoto(file, userId: number) {
    this.repository.update(userId, {
      photo: '\\x' + file.buffer.toString('hex'),
    });
  }

  public getPhoto(userId: number) {
    return this.repository
      .findOne(userId)
      .then(val => (val.photo ? Buffer.from(val.photo) : null));
  }

  public changeEmail(userId: number, newEmail: string) {
    this.repository.update(userId, {
      email: newEmail,
    });
  }

  public checkPassword(userId: number, password: string) {
    return this.get(userId).then(user => user.password === password);
  }

  public changePassword(userId: number, newPassword: string) {
    this.repository.update(userId, {
      password: newPassword,
    });
  }
}
