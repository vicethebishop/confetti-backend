import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './entity/user.entity';
import { UserService } from './service/user.service';
import { AuthModule } from '../auth/auth.module';
import { UserResolver } from './resolver/user.resolver';
import { UserController } from './controller/user/user.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserEntity]),
    forwardRef(() => AuthModule),
  ],
  controllers: [UserController],
  providers: [UserService, UserResolver],
  exports: [TypeOrmModule],
})
export class UserModule {}
